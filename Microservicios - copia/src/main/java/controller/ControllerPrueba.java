package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.Microservicios.model.ProductoDTO;


import service.ProductosService;

@Controller
public class ControllerPrueba {
	
	@Autowired
	ProductosService productosService;
	
	
	@RequestMapping("/productosPrueba/{id}")
	public String listaProductos3(Model model,@PathVariable("id") Long id) {
		
		ProductoDTO producto= productosService.getDetallesProducto(id);
        
		List<ProductoDTO> similares = productosService.getProductosSimilar(id);
		
		
		
		model.addAttribute("productosSimilares", similares);
		model.addAttribute("productos", producto);
		
		return "productos";
	}
}

package com.example.Microservicios.model;

import java.math.BigDecimal;

import lombok.Data;
import lombok.NonNull;


public class ProductoDTO {
	
	private Long id;

	private String name;
	
	private BigDecimal price;

	private Boolean availability;
	
	
	public ProductoDTO() {
		
	}

	public ProductoDTO(Long id, String name,  BigDecimal price,Boolean availability) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.availability = availability;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Boolean getAvailability() {
		return availability;
	}

	public void setAvailability(Boolean availability) {
		this.availability = availability;
	}
	
	
}

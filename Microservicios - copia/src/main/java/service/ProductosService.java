package service;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.example.Microservicios.model.ProductoDTO;





@Service
public class ProductosService {
	
	public ProductoDTO getDetallesProducto(Long id) {
		ProductoDTO producto = null;
	
		try {
			RestTemplate restTemplate = new RestTemplate();
			String apiUrl = "http://localhost:3001/product/" + id;
			ProductoDTO responseEntity = restTemplate.getForObject(apiUrl, ProductoDTO.class);
			ResponseEntity<ProductoDTO> response = restTemplate.getForEntity(apiUrl, ProductoDTO.class);
			HttpStatus statusCode = response.getStatusCode();
		    producto = response.getBody();
			
		}catch(Exception e) {
			producto = new ProductoDTO();
			producto.setId(id);
			producto.setName("not found");
		}
			
		return producto;
		
	}
	
	public List<ProductoDTO> getProductosSimilar(Long id) throws HttpClientErrorException {
		List<ProductoDTO> productosSimilares = new ArrayList<ProductoDTO>();
	
		RestTemplate restTemplate = new RestTemplate();
        String apiUrl = "http://localhost:3001/product/"+id+"/similarids";
        
        
        ResponseEntity<Long[]> response = restTemplate.getForEntity(apiUrl, Long[].class);
        HttpStatus statusCode = response.getStatusCode();
        Long[] similares=response.getBody();
       
        List<Long> list = Arrays.asList(similares);
        
      
		if (list.size() > 0) {

			for (Long idSimilar : list) {

				productosSimilares.add(getDetallesProducto(idSimilar));
			}
		}
		
        
       
        return productosSimilares;
	}
}
